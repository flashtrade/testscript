# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 16:06:43 2021

@author: Arijit
"""

from kiteconnect import KiteConnect
from kiteconnect import KiteTicker
import os
import pandas as pd
import csv
import logging
import sys

cwd = os.chdir("C:\\Users\91983")

WeeklyExpiry = '2021-09-02'

access_token = open("access_token.txt", 'r').read()
key_secret = open("api_key.txt", 'r').read().split()
kite = KiteConnect(api_key=key_secret[0])
kite.set_access_token(access_token)

instrument_dump = kite.instruments("NSE")
instrument_df = pd.DataFrame(instrument_dump)
instrument_df.to_csv("NSE_Instruments", index=False)

kws = KiteTicker(key_secret[0], kite.access_token)

order_variety = kite.VARIETY_REGULAR

order_id = None
sl_order_id = None
instrument_name = None
instrument_code = None


def get_instrument(by_direction=''):
    tradingsymbol = 'NIFTY BANK'
    if by_direction == "Bullish":
        OPT_TYPE = 'CE'
    else:
        OPT_TYPE = 'PE'
    ohlc = kite.ohlc('NSE:{}'.format(tradingsymbol))
    print('printing OHLC:',ohlc)
    ltp = ohlc['NSE:{}'.format(tradingsymbol)]['last_price']
    print('\n BANKNIFTY SPOT Price:',ltp)
    val = ltp / 100
    val = round(val,0)*100
#    abs_val = "{:.0f}".format(val) # to remove .0 string.
    OPT_STRIKE = "{}".format("{:.0f}".format(val + 0))
    print('Strike Price: '+OPT_STRIKE)
    
    instrument_name = None
    with open('instruments.csv','r') as csv_file:
        print('Step -1')
        csv_reader = csv.reader(csv_file)
        print('Step -2')
        next(csv_reader)        
        print('Step -3')
        for column in csv_reader:
            if column[6] == OPT_STRIKE and column[5] == WeeklyExpiry and column[9] == OPT_TYPE:
                instrument_name = str(column[2])
                instrument_code = column[0]
                print ('Found: '+instrument_name)
                #print instrument_name
                break
            
    return instrument_name, instrument_code
    

def get_option_current_price(instrument_name):
    ohlc = kite.ohlc('NFO:{}'.format(instrument_name))
    ltp = ohlc['NFO:{}'.format(instrument_name)]['last_price']
    return ltp
    
    

def place_trade(direction, paper_trade=False):
    global instrument_name, instrument_code
    instrument_name, instrument_code = get_instrument(direction)
    global order_id
    order_id = place_order(instrument_name, "Buy", 0, paper_trade)
    place_sl_order(paper_trade)
    
    
def place_order(symbl, buy_sell_flag, price=0, paper_trade = False):
    if buy_sell_flag == 'Buy':
       t_type = kite.TRANSACTION_TYPE_BUY
    elif buy_sell_flag == 'Sell':
       t_type = kite.TRANSACTION_TYPE_SELL
       
    if price == 0:
            prce = None
            o_type = kite.ORDER_TYPE_MARKET
    else:
            prce = price
            o_type = kite.ORDER_TYPE_SLM
            
    try:
        if paper_trade == False:            
            order_id = kite.place_order(tradingsymbol=symbl,variety=order_variety,
                                         exchange=kite.EXCHANGE_NFO,
                                         transaction_type=t_type,
                                         quantity=25,
                                         order_type=o_type,
                                         product=kite.PRODUCT_MIS,price=None, validity=None, 
                                         disclosed_quantity=None, trigger_price=prce, 
                                         squareoff=None, stoploss=None, trailing_stoploss=None, tag=None)         
            print("Order placed. ID is:", order_id)
        else:
            if price == 0:
                print("Order placed at: "+str(get_option_current_price(symbl)))
            else:
               print("SL Order placed at: "+str(get_option_current_price(symbl)))
            order_id = None
        return order_id
    except Exception as e:
        print("exception occured:" + str(e))
    

def check_for_order_fill(order_id):
    df = kite.order_history(order_id)
    count = len(df)
    o_status = "PENDING"
    for i in range(count):
        if df[i]['status'] == 'COMPLETE':
            o_status = "COMPLETE"
            buy_price = df[i]['average_price']
            sl_price = 0.9 * buy_price            
            break    
    return sl_price, o_status

def place_sl_order(paper_trade=False):
    order_status = 'PLACED'
    global sl_order_id, sl_price
    if paper_trade == False:
        while order_status != 'COMPLETE':
            sl_price,order_status = check_for_order_fill(order_id)        
       
    sl_order_id = place_order(instrument_name, "Sell", sl_price, paper_trade)
    startTicker()    
    

def on_connect(ws, response):
    logging.debug("con connect: {}".format(response))
    ws.subscribe(instrument_code)
    ws.set_mode(ws.MODE_FULL, instrument_code)
    
def on_close(ws, code, reason):
  print("Connection closed")

def on_error(ws, code, reason):
    print("Closed connection on error: {}".format(code, reason))
    
def on_ticks(ws, ticks):
    global sl_price
    for tick in ticks:
        if tick['last_price'] <= sl_price:
           stopTicker()
           print("Stoplss trigerred at "+str(get_option_current_price(instrument_name)))
        else:   
            diff = ((tick['last_price'] - sl_price) / sl_price) * 100
            if diff > 10:
                sl_price = sl_price * 1.05
                modify_order(sl_order_id, sl_price)
            
    
def modify_order(order_id, price):
        if order_id != None:
            prce, o_status = check_for_order_fill(order_id)
            if o_status != "COMPLETE":                
                    kite.modify_order(variety=order_variety,
                                             order_id=order_id,
                                             quantity=None,
                                             trigger_price=price 
                                         )
            else:
                    stopTicker()
                    print("Order already executed. Cannot modify")
        else:
            print("Current Stoploss price set to: "+str(get_option_current_price(instrument_name))) 
            

def startTicker():    
    kws.on_ticks=on_ticks
    kws.on_connect=on_connect
    kws.on_close=on_close
    kws.on_error=on_error
    kws.connect(threaded=True)
    
   
def stopTicker():
    kws.unsubscribe(instrument_code)
    #kws.stop()
    kws.close()
    sys.exit()
    

o_id = place_order("BANKNIFTY2190235600PE", "Sell", 255)



instrument_name, instrument_code = get_instrument()

    
order_id = place_order(instrument_name,"Buy", 0)

modify_order(o_id, 262.15)


#sl_order_id = check_for_order_fill(order_id)

buy_price = 0.0

            
sl_order_id, sl_price = place_sl_order()

    
instrument_name = "BANKNIFTY2190235600CE"
    
sl_order_id = place_order(instrument_name, "Sell", 300)
    
    






    


    
